'use strict';

var env = process.env.NODE_ENV;
var express = require('express');
var promise = require('bluebird');
var SwaggerExpress = require('swagger-express-mw');
var initSwagger = require('./api/helpers/commons');
var mongoose = require('./api/helpers/mongoose');

var config = {
  appRoot: __dirname // required config
};

var app = express();

app.locals.env = env;

app.set('view engine', 'html');

mongoose.connect();
initSwagger.setSwaggerOptions();

promise.promisifyAll(SwaggerExpress);

module.exports = SwaggerExpress.createAsync(config).then(function handleResponse(swaggerExpress) {

  // install middleware
  swaggerExpress.register(app);

  var port = process.env.PORT || 10010;
  app.listen(port);

  return app;
});
