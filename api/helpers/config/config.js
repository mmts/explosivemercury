'use strict';

var env = process.env.NODE_ENV;

var config = require('../../../env/' + (env || "develop"));

module.exports = config;