'use strict';

var mongoose = require('mongoose');
var config = require('../config').mongo;

var mongoHost = config.host || 'localhost';
var databaseName = config.database || 'explosiveMercury';

var promise = require('bluebird');
var mongoosePromise = promise.promisifyAll(mongoose);

module.exports = {
  connect: connect,
  getPromise: getPromise
};

function getPromise() {
  return mongoosePromise;
}

function connect() {
  mongoose.connect('mongodb://' + mongoHost + '/' + databaseName, function(err, res) {
    if (err) {
      console.log('ERROR: connecting to Database. ' + err);
    } else {
      console.log('Connected to Database');
    }
  });
}

