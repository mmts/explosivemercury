'use strict';

var debug = require('debug')('oauth');
var bcrypt = require('bcrypt');
var _ = require('lodash');
var promise = require('bluebird');


var config = require('./config').roles;
var mongoose = require('./mongoose');

promise.promisifyAll(bcrypt);


module.exports = {
  passwordCheck: passwordCheck,
  beforeCreate: beforeCreate
};

function beforeCreate(parsedBody, options, cb) {

  // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
  if (parsedBody.grant_type === 'password') {

    var username = parsedBody.username;
    var password = parsedBody.password;

    debug('Before create access token');

    debug('Validate user %s password', username);

    mongoose.getPromise().model('User').findAsync({email: username}).then(function(result){

      if (_.isEmpty(result)) {
        debug('User not found');
        return cb();
      }

      var preRoles = _.values(_.pick(config, result[0].role));

      parsedBody.scope = _.uniq(_.flatten(preRoles)).join(" ");

      options.attributes = {
        email: username,
        id: result[0].id,
        name: result[0].name,
        lastName: result[0].lastName,
        image: result[0].image,
        roles: result[0].role
      };

      bcrypt.compareAsync(password, result[0].password).then(function(check) {
        debug('Password check: %s', check);

        parsedBody.password = check ? true: "password";
        cb();
      });
    }).catch(cb);

  } else {

    options.attributes = parsedBody.attributes;
    cb();
  }
}

function passwordCheck(username, password, cb) {

  cb(null, password === true);
}
