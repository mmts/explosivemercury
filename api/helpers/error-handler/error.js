'use strict';

module.exports = errorFactory;

function isSequelizeError(error) {
  return (!!error.errmsg);
}

function formatSequelizeError(error) {
  if (!!error.code && error.code === 11000) {
    error.responseCode = 409;
  }
}

function errorFactory(res, next) {
  return function(err) {
    if (isSequelizeError(err)) {
      formatSequelizeError(err);
    }
    if (!!err.responseCode) {
      res.status(err.responseCode);

      delete err.responseCode;
    }
    next(err);
  };
}
