'use strict';

var _ = require('lodash');
var skill = require('../models/skill');
var commons = require('../helpers/commons/');
var errorHandler = require('../helpers/error-handler/');
var mongoose = require('../helpers/mongoose/').getPromise().model('Skill');

module.exports = {
    createSkill: createSkill,
    getSkills: getSkills,
    getSkill: getSkill,
    updateSkill: updateSkill
};

function createSkill(req, res, next) {

    var data = commons.pickParams(req);

    mongoose.createAsync(data)
      .then(res.send.bind(res))
      .catch(errorHandler(res, next));
}

function getSkills(req, res, next) {
    mongoose.findAsync()
        .then(function(result) {
            if (_.isEmpty(result)) {
                res.sendStatus(404);
            }else {
                res.send(result);
            }
        })
        .catch(errorHandler(res, next));
}

function getSkill(req, res, next) {
    var id = req.swagger.params.id.value;

    mongoose.findByIdAsync(id)
        .then(function(result) {
            if (_.isEmpty(result)) {
                res.sendStatus(404);
            }else {
                res.send(result);
            }
        })
        .catch(errorHandler(res, next));
}

function updateSkill(req, res, next) {

    var data = commons.pickParams(req);
    var id = req.swagger.params.id.value;

    mongoose.updateAsync({ _id : id}, { $set: data })
        .then(res.send.bind(res))
        .catch(errorHandler(res, next));
}