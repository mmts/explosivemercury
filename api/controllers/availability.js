'use strict';

var _ = require('lodash');
var commons = require('../helpers/commons/');
var availability = require('../models/availability');
var errorHandler = require('../helpers/error-handler/');
var mongoose = require('../helpers/mongoose/').getPromise().model('Availability') ;

module.exports = {
  getAvailability: getAvailability,
  getAvailabilities: getAvailabilities,
  createAvailability: createAvailability,
  updateAvailability: updateAvailability
};

function getAvailabilities(req, res, next) {
  mongoose.findAsync()
    .then(function(result) {
      if (_.isEmpty(result)) {
        res.sendStatus(404);
      }else {
        res.send(result);
      }
    })
    .catch(errorHandler(res, next));
}

function getAvailability(req, res, next) {
  var id = req.swagger.params.id.value;

  mongoose.findByIdAsync(id)
    .then(function(result) {
      if (_.isEmpty(result)) {
        res.sendStatus(404);
      }else {
        res.send(result);
      }
    })
    .catch(errorHandler(res, next));
}

function createAvailability(req, res, next) {

  var data = commons.pickParams(req);

  mongoose.createAsync(data)
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}

function updateAvailability(req, res, next) {

  var data = commons.pickParams(req);
  var id = req.swagger.params.id.value;

  mongoose.updateAsync({ _id: id },{$set: data})
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}

