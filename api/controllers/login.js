'use strict';

var oauth = require('../helpers/oauth/');
var errorHandler = require('../helpers/error-handler/');

module.exports = {
  verifyToken: verifyToken,
  refreshToken: refreshToken,
  login: login
};

function refreshToken(req, res, next) {

  // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
  var token = req.body.refresh_token;

  oauth.refreshToken(token)
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}

function verifyToken(req, res, next) {

  // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
  var accessToken = req.headers.authorization;
  var scopes = req.query.scopes;

  oauth.verifyToken(accessToken, scopes || '')
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}


function login(req, res, next) {

  var email = req.body.email;
  var password = req.body.password;

  oauth.login(email, password)
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}