'use strict';

var _ = require('lodash');
var skillLevel = require('../models/levelSkill');
var commons = require('../helpers/commons/');
var errorHandler = require('../helpers/error-handler/');
var mongoose = require('../helpers/mongoose/').getPromise().model('LevelSkill');

module.exports = {
  createSkillLevel: createSkillLevel,
  getSkillLevels: getSkillLevels,
  getSkillLevel: getSkillLevel,
  updateSkillLevel: updateSkillLevel
};

function createSkillLevel(req, res, next) {

  var data = commons.pickParams(req);

  mongoose.createAsync(data)
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}

function getSkillLevels(req, res, next) {
  mongoose.findAsync()
    .then(function(result) {
      if (_.isEmpty(result)) {
        res.sendStatus(404);
      }else {
        res.send(result);
      }
    })
    .catch(errorHandler(res, next));
}

function getSkillLevel(req, res, next) {
  var id = req.swagger.params.id.value;

  mongoose.findByIdAsync(id)
    .then(function(result) {
      if (_.isEmpty(result)) {
        res.sendStatus(404);
      }else {
        res.send(result);
      }
    })
    .catch(errorHandler(res, next));
}

function updateSkillLevel(req, res, next) {

  var data = commons.pickParams(req);
  var id = req.swagger.params.id.value;

  mongoose.updateAsync({ _id : id}, { $set: data })
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}