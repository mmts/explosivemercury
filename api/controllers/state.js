'use strict';

var _ = require('lodash');
var commons = require('../helpers/commons/');
var state = require('../models/state');
var errorHandler = require('../helpers/error-handler/');
var mongoose = require('../helpers/mongoose/').getPromise().model('State') ;

module.exports = {
  getState: getState,
  getStates: getStates,
  createState: createState,
  updateState: updateState
};

function getStates(req, res, next) {
  mongoose.findAsync()
    .then(function(result) {
      if (_.isEmpty(result)) {
        res.sendStatus(404);
      }else {
        res.send(result);
      }
    })
    .catch(errorHandler(res, next));
}

function getState(req, res, next) {
  var id = req.swagger.params.id.value;

  mongoose.findByIdAsync(id)
    .then(function(result) {
      if (_.isEmpty(result)) {
        res.sendStatus(404);
      }else {
        res.send(result);
      }
    })
    .catch(errorHandler(res, next));
}

function createState(req, res, next) {

  var data = commons.pickParams(req);

  mongoose.createAsync(data)
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}

function updateState(req, res, next) {

  var data = commons.pickParams(req);
  var id = req.swagger.params.id.value;

  mongoose.updateAsync({ _id: id },{$set: data})
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}

