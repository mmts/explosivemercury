'use strict';

var _ = require('lodash');
var category = require('../models/category');
var commons = require('../helpers/commons/');
var errorHandler = require('../helpers/error-handler/');
var mongoose = require('../helpers/mongoose/').getPromise().model('Category');

module.exports = {
  createCategory: createCategory,
  getCategories: getCategories,
  getCategory: getCategory,
  updateCategory: updateCategory
};

function createCategory(req, res, next) {

  var data = commons.pickParams(req);

  mongoose.createAsync(data)
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}

function getCategories(req, res, next) {
  mongoose.findAsync()
    .then(function(result) {
      if (_.isEmpty(result)) {
        res.sendStatus(404);
      }else {
        res.send(result);
      }
    })
    .catch(errorHandler(res, next));
}

function getCategory(req, res, next) {
  var id = req.swagger.params.id.value;

  mongoose.findByIdAsync(id)
    .then(function(result) {
      if (_.isEmpty(result)) {
        res.sendStatus(404);
      }else {
        res.send(result);
      }
    })
    .catch(errorHandler(res, next));
}

function updateCategory(req, res, next) {

  var data = commons.pickParams(req);
  var id = req.swagger.params.id.value;

  mongoose.updateAsync({ _id : id}, { $set: data })
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}
