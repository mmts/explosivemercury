'use strict';

var _ = require('lodash');
var commons = require('../helpers/commons/');
var educationType = require('../models/educationType');
var errorHandler = require('../helpers/error-handler/');
var mongoose = require('../helpers/mongoose/').getPromise().model('EducationType') ;

module.exports = {
  createEducationType: createEducationType,
  getEducationTypes: getEducationTypes,
  getEducationTypeById: getEducationTypeById,
  updateEducationTypeById: updateEducationTypeById
};

function createEducationType(req, res, next) {

  var data = commons.pickParams(req);

  mongoose.createAsync(data)
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}

function getEducationTypes(req, res, next) {
  mongoose.findAsync()
    .then(function(result) {
      if (_.isEmpty(result)) {
        res.sendStatus(404);
      }else {
        res.send(result);
      }
    })
    .catch(errorHandler(res, next));
}

function getEducationTypeById(req, res, next) {
  var id = req.swagger.params.id.value;

  mongoose.findByIdAsync(id)
    .then(function(result) {
      if (_.isEmpty(result)) {
        res.sendStatus(404);
      }else {
        res.send(result);
      }
    })
    .catch(errorHandler(res, next));
}

function updateEducationTypeById(req, res, next) {
  var id = req.swagger.params.id.value;
  var data = commons.pickParams(req);

  mongoose.updateAsync({_id: id}, {$set: data})
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}