'use strict';

var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var categorySchema = new Schema({
  name:           { type: String, index: {unique: true, dropDups: true}, require: true },
  description:    { type: String },
  skills:         [{ type: Schema.ObjectId, ref: "Skill"}],
  status:         { type: Boolean, require: true }
});

var Category = mongoose.model('Category', categorySchema);

module.exports = Category;
