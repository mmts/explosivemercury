'use strict';

var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var stateSchema = new Schema({
  name:       { type: String, index: {unique: true, dropDups: true} },
  cities:     [{ type: Schema.ObjectId, ref: "City" }],
  status:     { type: Boolean }
});

var State = mongoose.model('State', stateSchema);

module.exports = State;
