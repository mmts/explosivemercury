'use strict';

var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var levelSkillSchema = new Schema({
  name:           { type: String, index: {unique: true, dropDups: true}, required: true },
  description:    { type: String },
  status:         { type: Boolean, required: true }
});

var LevelSkill = mongoose.model('LevelSkill', levelSkillSchema);

module.exports = LevelSkill;
