'use strict';

var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var roleSchema = new Schema({
  name:           { type: String,  index: {unique: true, dropDups: true}, required: true},
  status:         { type: Boolean, required: true }
});

var Role = mongoose.model('Role', roleSchema);

module.exports = Role;
