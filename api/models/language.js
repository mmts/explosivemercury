'use strict';

var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var languageSchema = new Schema({
  name:   { type: String, index: {unique: true, dropDups: true}, required: true },
  status: { type: Boolean, required: true }
});

var Language = mongoose.model('Language', languageSchema);

module.exports = Language;
