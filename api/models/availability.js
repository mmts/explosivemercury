'use strict';

var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var availabilitySchema = new Schema({
  name:           { type: String, index: {unique: true, dropDups: true} },
  description:    { type: String },
  status:         { type: Boolean }
});

var Availability = mongoose.model('Availability', availabilitySchema);

module.exports = Availability;

