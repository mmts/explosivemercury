'use strict';

var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var skillSchema = new Schema({
  name:           { type: String, index: {unique: true, dropDups: true}, require: true },
  description:    { type: String },
  status:         { type: Boolean, require: true }
});

var Skill = mongoose.model('Skill', skillSchema);

module.exports = Skill;
