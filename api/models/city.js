'use strict';

var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var citySchema = new Schema({
  name:   { type: String , index: {unique: true, dropDups: true} },
  status: { type: Boolean }
});

var City = mongoose.model('City', citySchema);

module.exports = City;
