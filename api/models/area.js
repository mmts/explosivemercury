'use strict';

var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var areaSchema = new Schema({
  name:           { type: String, index: {unique: true, dropDups: true}, require: true },
  description:    { type: String },
  categories:     [{ type: Schema.ObjectId, ref: "Category" }],
  status:         { type: Boolean, require: true }
});

var Area = mongoose.model('Area', areaSchema);

module.exports = Area;

