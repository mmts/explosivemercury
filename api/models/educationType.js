'use strict';

var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var educationTypeSchema = new Schema({
  name:           { type: String, index: {unique: true, dropDups: true}, required: true },
  description:    { type: String, required: true },
  status:         { type: Boolean, required: true }
});

var EducationType = mongoose.model('EducationType', educationTypeSchema);

module.exports = EducationType;
