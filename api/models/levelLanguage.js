'use strict';

var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var levelLanguageSchema = new Schema({
  name:   { type: String, index: {unique: true, dropDups: true}, required: true },
  status: { type: Boolean, required: true }
});

var LevelLanguage = mongoose.model('LevelLanguage', levelLanguageSchema);

module.exports = LevelLanguage;