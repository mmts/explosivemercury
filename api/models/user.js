'use strict';

var mongoose = require('mongoose');
var bcrypt = require('mongoose-bcrypt');

var Schema   = mongoose.Schema;

var userSchema = new Schema({
  email:          { type: String, index: {unique: true, dropDups: true }},
  password:       { type: String, required: true },
  name:           { type: String, required: true },
  lastName:       { type: String, required: true },
  documentId:     { type: String, required: true },
  typeDocumentId: { type: String, required: true },
  image:          { type: String },
  role:           [{type: String }],
  status:         { type: Boolean }
});

userSchema.plugin(bcrypt);

var User = mongoose.model('User', userSchema);

module.exports = User;
