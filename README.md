# Base api with swagger and oauth2 #

## Requirements: ##

  - Redis:
      - OS X:
          - brew install redis-server
      - Ubuntu:
          - deb http://packages.dotdeb.org squeeze all
          - deb-src http://packages.dotdeb.org squeeze all
          - wget -q -O - http://www.dotdeb.org/dotdeb.gpg | sudo apt-key add -
          - sudo apt-get update
          - sudo apt-get install redis-server
         
        
- [Node](https://nodejs.org/en/)
    
- Swagger:
    - npm install -g swagger
    
## Get Started ##


### Clone Repo ###

     git https://github.com/TeravisionTechnologies/angular-demo.git


### Install dependencies ###

     cd explosivemercury
     npm install

### Copy environment file ###
    
     cp env/template.json env/develop.json

### Run Init-Redis ###

     node init-redis.js


### Execute project the first time ###

     node app.js


### And then you can have live reload ###

     swagger project 
or

     DEBUG=* swagger project start
